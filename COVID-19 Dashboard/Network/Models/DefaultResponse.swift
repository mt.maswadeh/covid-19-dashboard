//
//  DefaultResponse.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 17/05/2021.
//

import Foundation

protocol ResponseStatus:Codable {
    var isSuccess:Bool? { get set }
    var errors:[Error]? { get set }
}

struct DefaultResponse:ResponseStatus {
    var isSuccess:Bool?
    var errors:[Error]?
}
