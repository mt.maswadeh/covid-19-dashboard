//
//  Error.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 17/05/2021.
//

import Foundation

struct Error:Codable {
    var code:Int?
    var description:String?
}
