//
//  APIFile.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 17/05/2021.
//

import UIKit

struct APIFile: Codable {
    let name:String //API Key
    let fileName:String
    let data:Data
    let mimeType:String
}
