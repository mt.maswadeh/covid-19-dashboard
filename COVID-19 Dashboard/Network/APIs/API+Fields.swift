//
//  API+Fields.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 17/05/2021.
//
import Foundation
import Alamofire

extension API {
    
    var method : HTTPMethod {
        switch self {
        case .getData,.getCountryDetails,.getNews:
            return HTTPMethod.get
        }
    }
    
    var header : [String:String]? {
  
    return ["Content-type": "application/x-www-form-urlencoded"]
    
    }
    
    var parameter : [String:Any]? {
        switch self {
        case .getData(let dateFrom, let dateTo):
            return ["date_from":dateFrom,"date_to":dateTo]
            
        case .getCountryDetails(let name):
            return ["name":name]
            
        case .getNews(let country,let category):
            return ["country":country,"category":category]
        }
    }
    
    var files : [APIFile]? {
        switch self {
        default:
            return nil
        }
    }
}
