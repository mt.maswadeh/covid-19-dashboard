//
//  API+Configurations.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 17/05/2021.
//
import Foundation
import Alamofire

extension API {
    
    var parameterEncoding : ParameterEncoding {
        switch self {
        default:
            return URLEncoding.default //JSONEncoding.default
        }
    }

    var authenticate : URLCredential? {
        switch self {
        default:
            return nil
        }
    }
    
    var timeout : TimeInterval? {
        switch self {
        default:
            return 120
        }
    }
    
    var ignorCache : Bool {
        switch self {
        default:
            return true
        }
    }
    
    var skipInvalidCertificate : Bool {
        switch self {
        default:
            return true
        }
    }
    
}
