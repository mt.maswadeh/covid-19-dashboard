//
//  API.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 17/05/2021.
//

import Foundation

enum API {
    static let trackingDomain = "https://api.covid19tracking.narrativa.com/"
    static let countryDetailsDomain = "https://restcountries.eu/rest/v2/name/"
    static let newsDomain = "https://newsapi.org/v2/top-headlines?"
    static let apiKey = "c1540d8eb438483eb11a900f353b2f99"
    
    case getData(dateFrom:String,dateTo:String)
    case getCountryDetails(name:String)
    case getNews(country:String,category:String)


    var url : URL? {
        var url = ""
        switch self {
        case .getData(let dateFrom,let dateTo):
            url =  API.trackingDomain.appending("api?date_from=\(dateFrom)&date_to=\(dateTo)")
        case .getCountryDetails(let name):
            url =  API.countryDetailsDomain.appending("\(name)?fullText=true")
        case .getNews(let country,let category):
            url = API.newsDomain.appending("country=\(country)&category=\(category)&apiKey=\(API.apiKey)")

            
        }
        return URL.init(string: url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? "")
    }
    
}
