//
//  MapViewController.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 17/05/2021.
//

import UIKit
import GoogleMaps
import Alamofire
import ProgressHUD
import BottomPopup
class MapViewController: UIViewController,GMSMapViewDelegate {
    
    var datesArray = [Dates]()
    var jordanObject = Country()
    var uaeObject = Country()
    var ksaObject = Country()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupMap()
        dashboardDataRequest()
        // Do any additional setup after loading the view.
    }
    
    func setupMap() {
        
        let camera = GMSCameraPosition.camera(withLatitude: 24.266906, longitude: 45.107849, zoom: 4.0)
        let mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        mapView.delegate = self
        self.view.addSubview(mapView)
        
        // Creates a marker in the center of the map.
        let jordanMarker = GMSMarker()
        jordanMarker.position = CLLocationCoordinate2D(latitude: 31.963158, longitude: 35.930359)
        jordanMarker.title = "Amman"
        jordanMarker.snippet = "Jordan"
        jordanMarker.map = mapView
        
        let uaeMarker = GMSMarker()
        uaeMarker.position = CLLocationCoordinate2D(latitude: 25.276987, longitude: 55.296249)
        uaeMarker.title = "Dubai"
        uaeMarker.snippet = "United Arab Emirates"
        uaeMarker.map = mapView
        
        let ksaMarker = GMSMarker()
        ksaMarker.position = CLLocationCoordinate2D(latitude: 24.266906, longitude: 45.107849)
        ksaMarker.title = "Riyadh"
        ksaMarker.snippet = "Saudi Arabia"
        ksaMarker.map = mapView
        
       
    }
    
    func dashboardDataRequest() {
        ProgressHUD.show()
        
        
        let date = Date().dayBefore
        let formattedDate = date.getFormattedDate(format: "yyyy-MM-dd") // Set output formate

   
        APIClient.request(api: API.getData(dateFrom: formattedDate, dateTo: formattedDate)) {(dashboardData:Dashboard?, response:DataResponse<Data>?, cache:APIClient.DataStatus) in
            
            let dateObject = dashboardData!.dates.datesArray[formattedDate]!
            self.jordanObject = dateObject.countries.countriesArray["Jordan"]!
            self.uaeObject = dateObject.countries.countriesArray["United Arab Emirates"]!
            self.ksaObject = dateObject.countries.countriesArray["Saudi Arabia"]!

            ProgressHUD.dismiss()
            
        }
    }
    
    // tap map marker
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if marker.title == "Amman" {
            self.openDetailsScreen(countryObject: jordanObject)
        }
        else if  marker.title == "Dubai" {
            self.openDetailsScreen(countryObject: uaeObject)
        }
        else {
            self.openDetailsScreen(countryObject: ksaObject)
        }
        
        return true
    }
    
    func openDetailsScreen(countryObject:Country) {
        guard let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardDetailsViewController") as? DashboardDetailsViewController else { return }
        vc.countryObject = countryObject
        vc.popupDelegate = self
        
        present(vc, animated: true, completion: nil)
    }

}


extension MapViewController: BottomPopupDelegate {
    
    func bottomPopupViewLoaded() {
        print("bottomPopupViewLoaded")
    }
    
    func bottomPopupWillAppear() {
        print("bottomPopupWillAppear")
    }
    
    func bottomPopupDidAppear() {
        print("bottomPopupDidAppear")
    }
    
    func bottomPopupWillDismiss() {
        print("bottomPopupWillDismiss")
    }
    
    func bottomPopupDidDismiss() {
        print("bottomPopupDidDismiss")
    }
    
    func bottomPopupDismissInteractionPercentChanged(from oldValue: CGFloat, to newValue: CGFloat) {
        print("bottomPopupDismissInteractionPercentChanged fromValue: \(oldValue) to: \(newValue)")
    }
}


extension Date {
   func getFormattedDate(format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: self)
    }
    
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}


