//
//  DashboardDetailsViewController.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 18/05/2021.
//

import UIKit
import BottomPopup

class DashboardDetailsViewController: BottomPopupViewController {
    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    var countryObject = Country()
    @IBOutlet weak var newLabel: UILabel!
    @IBOutlet weak var deathsLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var revoceredLabel: UILabel!
    
    override var popupHeight: CGFloat { return height ?? CGFloat(400) }
    
    override var popupTopCornerRadius: CGFloat { return topCornerRadius ?? CGFloat(10) }
    
    override var popupPresentDuration: Double { return presentDuration ?? 0.2 }
    
    override var popupDismissDuration: Double { return dismissDuration ?? 0.2 }
    
    override var popupShouldDismissInteractivelty: Bool { return shouldDismissInteractivelty ?? true }
    
    override var popupDimmingViewAlpha: CGFloat { return 0.5 }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        // Do any additional setup after loading the view.
    }
    
    func fetchData() {
        countryLabel.text = countryObject.name
        newLabel.text = countryObject.todayNewOpenCases?.description
        deathsLabel.text = countryObject.todayNewDeaths?.description
        revoceredLabel.text = countryObject.todayNewRecovered?.description
    }
    @IBAction func dismissButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
