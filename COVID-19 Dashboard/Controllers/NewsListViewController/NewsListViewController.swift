//
//  NewsListViewController.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 18/05/2021.
//

import UIKit
import Alamofire
import ProgressHUD
class NewsListViewController: UIViewController {

    var newsObject:News?
    var countryDetails:CountryDetail?
    var articles:[Article]?
    @IBOutlet weak var articlesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "\(countryDetails?.name ?? "") News"
        self.articlesTableView.delegate = self
        self.articlesTableView.dataSource = self
        self.articlesTableView.rowHeight = UITableView.automaticDimension
        self.articlesTableView.estimatedRowHeight = 90
        self.articlesTableView.tableFooterView = UIView()

        newsDataRequest(countryCode: countryDetails?.alpha2Code ?? "")

    }
    
    func newsDataRequest(countryCode:String) {
        ProgressHUD.show()
        APIClient.request(api: API.getNews(country: countryCode, category: "health")) {(news:News?, response:DataResponse<Data>?, cache:APIClient.DataStatus) in
            
            ProgressHUD.dismiss()
            self.newsObject = news
            self.articles = news?.articles
            if self.articles?.count ?? 0 > 0 {
                self.articlesTableView.reloadData()
            }
            else {
                ProgressHUD.showError("There is no news")
            }
            
        }
        
    }
    
}

extension NewsListViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articles?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsViewCell", for: indexPath) as! NewsViewCell
        
        cell.fetchNew(article: (articles?[indexPath.row])!)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let article = articles?[indexPath.row]
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewsDetailsViewController") as? NewsDetailsViewController
        vc?.article = article
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
