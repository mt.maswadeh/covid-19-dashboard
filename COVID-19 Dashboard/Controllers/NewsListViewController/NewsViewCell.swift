//
//  NewsViewCell.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 18/05/2021.
//

import UIKit
import SDWebImage
class NewsViewCell: UITableViewCell {

    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsAuthorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func fetchNew(article:Article) {
        newsImage.sd_setImage(with: URL(string: article.urlToImage ?? ""), placeholderImage: UIImage(named: "LaunchIcon"))
        self.newsTitleLabel.text = article.title
        self.newsAuthorLabel.text = article.author
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
