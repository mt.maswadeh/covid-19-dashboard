//
//  CountryViewCell.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 18/05/2021.
//

import UIKit

class CountryViewCell: UITableViewCell {

    @IBOutlet weak var countryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
