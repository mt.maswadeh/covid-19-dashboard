//
//  ListViewController.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 17/05/2021.
//

import UIKit
import Alamofire
import ProgressHUD
class ListViewController: UIViewController {
    
    @IBOutlet weak var countriesTableView: UITableView!
    var datesArray = [Dates]()
    var countriesArray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.countriesTableView.delegate = self
        self.countriesTableView.dataSource = self
        self.countriesTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dashboardDataRequest()
    }
    
    func dashboardDataRequest() {
        ProgressHUD.show()
        
        APIClient.request(api: API.getData(dateFrom: "2020-10-20", dateTo: "2020-10-20")) {(dashboardData:Dashboard?, response:DataResponse<Data>?, cache:APIClient.DataStatus) in
            
            let dateObject = dashboardData!.dates.datesArray["2020-10-20"]!
            self.countriesArray = Array(dateObject.countries.countriesArray.keys)
            ProgressHUD.dismiss()
            self.countriesTableView.reloadData()
            
        }
    }
    
    func countriesDataRequest(countryName:String) {
        ProgressHUD.show()
        APIClient.request(api: API.getCountryDetails(name: countryName)) {(country:[CountryDetail]?, response:DataResponse<Data>?, cache:APIClient.DataStatus) in
            
            print(country?[0].alpha2Code ?? "us")
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewsListViewController") as? NewsListViewController
            vc?.countryDetails = country?[0]
            self.navigationController?.pushViewController(vc!, animated: true)
            ProgressHUD.dismiss()
            
        }
        
    }
}

extension ListViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countriesArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryViewCell", for: indexPath) as! CountryViewCell
    
        cell.countryName.text = countriesArray[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let countryName = countriesArray[indexPath.row]
        countriesDataRequest(countryName: countryName)
    }
    
    
}
