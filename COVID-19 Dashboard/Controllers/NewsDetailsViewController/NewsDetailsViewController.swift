//
//  NewsDetailsViewController.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 18/05/2021.
//

import UIKit

class NewsDetailsViewController: UIViewController {

    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var articleTextView: UITextView!
    var article:Article?

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData(article: article!)
        // Do any additional setup after loading the view.
    }
    
    func fetchData(article:Article) {
        newsTitleLabel.text = article.title
        articleTextView.text = "\(article.articleDescription ?? "") \n \(article.url ?? "")"
        articleImage.sd_setImage(with: URL(string: article.urlToImage ?? ""), placeholderImage: UIImage(named: "LaunchIcon"))
    }

}
