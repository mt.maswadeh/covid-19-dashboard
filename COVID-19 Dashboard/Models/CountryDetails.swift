//
//  CountryDetails.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 18/05/2021.
//

import Foundation

// MARK: - CountryDetail
struct CountryDetail: Codable {
    let name: String?
    let topLevelDomain: [String]?
    let alpha2Code: String?
    let alpha3Code: String?
    let callingCodes: [String]?
    let capital: String?
    let altSpellings: [String]?
    let region: String?
    let subregion: String?
    let population: Int?
    let latlng: [Int]?
    let demonym: String?
    let area: Int?
    let gini: Double?
    let timezones: [String]?
    let borders: [String]?
    let nativeName: String?
    let numericCode: String?
    let currencies: [Currency]?
    let languages: [Language]?
    let translations: Translations?
    let flag: String?
    let regionalBlocs: [RegionalBloc]?
    let cioc: String?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case topLevelDomain = "topLevelDomain"
        case alpha2Code = "alpha2Code"
        case alpha3Code = "alpha3Code"
        case callingCodes = "callingCodes"
        case capital = "capital"
        case altSpellings = "altSpellings"
        case region = "region"
        case subregion = "subregion"
        case population = "population"
        case latlng = "latlng"
        case demonym = "demonym"
        case area = "area"
        case gini = "gini"
        case timezones = "timezones"
        case borders = "borders"
        case nativeName = "nativeName"
        case numericCode = "numericCode"
        case currencies = "currencies"
        case languages = "languages"
        case translations = "translations"
        case flag = "flag"
        case regionalBlocs = "regionalBlocs"
        case cioc = "cioc"
    }
}

// MARK: - Currency
struct Currency: Codable {
    let code: String?
    let name: String?
    let symbol: String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case name = "name"
        case symbol = "symbol"
    }
}

// MARK: - Language
struct Language: Codable {
    let iso6391: String?
    let iso6392: String?
    let name: String?
    let nativeName: String?

    enum CodingKeys: String, CodingKey {
        case iso6391 = "iso639_1"
        case iso6392 = "iso639_2"
        case name = "name"
        case nativeName = "nativeName"
    }
}

// MARK: - RegionalBloc
struct RegionalBloc: Codable {
    let acronym: String?
    let name: String?

    enum CodingKeys: String, CodingKey {
        case acronym = "acronym"
        case name = "name"
    }
}

// MARK: - Translations
struct Translations: Codable {
    let de: String?
    let es: String?
    let fr: String?
    let ja: String?
    let it: String?
    let br: String?
    let pt: String?
    let nl: String?
    let hr: String?
    let fa: String?

    enum CodingKeys: String, CodingKey {
        case de = "de"
        case es = "es"
        case fr = "fr"
        case ja = "ja"
        case it = "it"
        case br = "br"
        case pt = "pt"
        case nl = "nl"
        case hr = "hr"
        case fa = "fa"
    }
}
