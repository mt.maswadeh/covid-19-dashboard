//
//  Dashboard.swift
//  COVID-19 Dashboard
//
//  Created by Mohammad on 17/05/2021.
//

import Foundation


struct Dashboard: Codable {
    var dates: Dates
    var metadata: Metadata
    var total: Total
    var updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case dates = "dates"
        case metadata = "metadata"
        case total = "total"
        case updatedAt = "updated_at"

    }
}

struct Dates: Codable {
    var datesArray: [String: DateObject]


    private struct CustomCodingKeys: CodingKey {
        var stringValue: String
        init?(stringValue: String) {
            self.stringValue = stringValue
        }
        var intValue: Int?
        init?(intValue: Int) {
            return nil
        }
    }
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CustomCodingKeys.self)
        
        self.datesArray = [String: DateObject]()
        for key in container.allKeys {
            let value = try container.decode(DateObject.self, forKey: CustomCodingKeys(stringValue: key.stringValue)!)
            self.datesArray[key.stringValue] = value
        }
    }
}

public struct DateObject: Codable {
    public var countries: Countries
    public var info: Info
    
   public struct Countries: Codable {
       public var countriesArray: [String: Country]
       
       private struct CustomCodingKeys: CodingKey {
           var stringValue: String
           init?(stringValue: String) {
               self.stringValue = stringValue
           }
           var intValue: Int?
           init?(intValue: Int) {
               return nil
           }
       }
       public init(from decoder: Decoder) throws {
           let container = try decoder.container(keyedBy: CustomCodingKeys.self)
           
           self.countriesArray = [String: Country]()
           for key in container.allKeys {
               let value = try container.decode(Country.self, forKey: CustomCodingKeys(stringValue: key.stringValue)!)
               self.countriesArray[key.stringValue] = value
           }
       }
   }

}

// MARK: - Contry
public struct Country: Codable {
    var date: String?
    var id: String?
    var links: [Link]?
    var name: String?
    var nameEs: String?
    var nameIt: String?
    var source: String?
    var todayConfirmed: Int?
    var todayDeaths: Int?
    var todayNewConfirmed: Int?
    var todayNewDeaths: Int?
    var todayNewOpenCases: Int?
    var todayNewRecovered: Int?
    var todayOpenCases: Int?
    var todayRecovered: Int?
    var todayVsYesterdayConfirmed: Double?
    var todayVsYesterdayDeaths: Double?
    var todayVsYesterdayOpenCases: Double?
    var todayVsYesterdayRecovered: Double?
    var yesterdayConfirmed: Int?
    var yesterdayDeaths: Int?
    var yesterdayOpenCases: Int?
    var yesterdayRecovered: Int?

    enum CodingKeys: String, CodingKey {
        case date = "date"
        case id = "id"
        case links = "links"
        case name = "name"
        case nameEs = "name_es"
        case nameIt = "name_it"
        case source = "source"
        case todayConfirmed = "today_confirmed"
        case todayDeaths = "today_deaths"
        case todayNewConfirmed = "today_new_confirmed"
        case todayNewDeaths = "today_new_deaths"
        case todayNewOpenCases = "today_new_open_cases"
        case todayNewRecovered = "today_new_recovered"
        case todayOpenCases = "today_open_cases"
        case todayRecovered = "today_recovered"
        case todayVsYesterdayConfirmed = "today_vs_yesterday_confirmed"
        case todayVsYesterdayDeaths = "today_vs_yesterday_deaths"
        case todayVsYesterdayOpenCases = "today_vs_yesterday_open_cases"
        case todayVsYesterdayRecovered = "today_vs_yesterday_recovered"
        case yesterdayConfirmed = "yesterday_confirmed"
        case yesterdayDeaths = "yesterday_deaths"
        case yesterdayOpenCases = "yesterday_open_cases"
        case yesterdayRecovered = "yesterday_recovered"
    }
}


// MARK: - Metadata
struct Metadata: Codable {
    let by: String?
    let url: [String]?

    enum CodingKeys: String, CodingKey {
        case by = "by"
        case url = "url"
    }
}

// MARK: - Total
struct Total: Codable {
    let date: String?
    let name: String?
    let nameEs: String?
    let nameIt: String?
    let rid: String?
    let source: String?
    let todayConfirmed: Int?
    let todayDeaths: Int?
    let todayNewConfirmed: Int?
    let todayNewDeaths: Int?
    let todayNewOpenCases: Int?
    let todayNewRecovered: Int?
    let todayOpenCases: Int?
    let todayRecovered: Int?
    let todayVsYesterdayConfirmed: Double?
    let todayVsYesterdayDeaths: Double?
    let todayVsYesterdayOpenCases: Double?
    let todayVsYesterdayRecovered: Double?
    let yesterdayConfirmed: Int?
    let yesterdayDeaths: Int?
    let yesterdayOpenCases: Int?
    let yesterdayRecovered: Int?

    enum CodingKeys: String, CodingKey {
        case date = "date"
        case name = "name"
        case nameEs = "name_es"
        case nameIt = "name_it"
        case rid = "rid"
        case source = "source"
        case todayConfirmed = "today_confirmed"
        case todayDeaths = "today_deaths"
        case todayNewConfirmed = "today_new_confirmed"
        case todayNewDeaths = "today_new_deaths"
        case todayNewOpenCases = "today_new_open_cases"
        case todayNewRecovered = "today_new_recovered"
        case todayOpenCases = "today_open_cases"
        case todayRecovered = "today_recovered"
        case todayVsYesterdayConfirmed = "today_vs_yesterday_confirmed"
        case todayVsYesterdayDeaths = "today_vs_yesterday_deaths"
        case todayVsYesterdayOpenCases = "today_vs_yesterday_open_cases"
        case todayVsYesterdayRecovered = "today_vs_yesterday_recovered"
        case yesterdayConfirmed = "yesterday_confirmed"
        case yesterdayDeaths = "yesterday_deaths"
        case yesterdayOpenCases = "yesterday_open_cases"
        case yesterdayRecovered = "yesterday_recovered"
    }
}

// MARK: - Link
struct Link: Codable {
    let href: String?
    let rel: String?
    let type: String?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case rel = "rel"
        case type = "type"
    }
}

// MARK: - Info
public struct Info: Codable {
    let date: String?
    let dateGeneration: String?
    let yesterday: String?

    enum CodingKeys: String, CodingKey {
        case date = "date"
        case dateGeneration = "date_generation"
        case yesterday = "yesterday"
    }
}
